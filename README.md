# Wordpress 2.1

This runs a Wordpress instance - I use it for my blog so I consider it as production ready.

If you have more generic questions please have a look [here](https://cms.manhart.space/dockerisierung)

## Getting Started

For starting the service initially just copy the `.env-example` to `.env` and edit the variables so they fit your environment.

After that, you can start the container.

### Manual steps

If you want to install / update plugins you have to add `define('FS_METHOD','direct');` to `wp-config.php` (see open issues).

		# docker exec -ti YOUR_WP_CONTAINER sh
		# echo "define('FS_METHOD','direct');" >>wp-config.php

### Build / Extend

Please understand what I did with the volumes before changing configurations.

I created 3 host volume mounts in the container for the resources I want to survive upgrades / container rebuilts:

    ./data/wordpress/themes
    ./data/wordpress/plugins
    ./data/wordpress/uploads

I also created a named volume for wordpress to copy its files into

    wordpress-volume

Nginx will also use the same volume mounts so it can serve all static resources.

### Replicating the service

1. Copy all the host mounted volumes (LOCAL_THEMES_PATH, LOCAL_PLUGINS_PATH, LOCAL_UPLOADS_PATH, LOCAL_DB_PATH) to the new service location (or copy the whole service).

2. Edit the LOCAL_NGINX_PORT to something else if you clone onto the same host system.

3. Start the containers.

4. Connect to the db container and edit the wp_options (replace YOUR_SITE_URL with the real site url):

		# docker exec -ti wordpress_db_1 sh
		# mysql $MYSQL_DATABASE -u $MYSQL_USER -p$MYSQL_PASSWORD
		MariaDB [...]> update wp_options set option_value='YOUR_SITE_URL' where option_name in ('siteurl', 'home');
		MariaDB [...]> select * from wp_options where option_name in ('siteurl', 'home');

5. Optional: If you have an(other) nginx upfront, you need to configure it accordingly.

6. Restart the service (just to be sure :) )

		# docker-compose restart

7. Append the neccessary config for upgrading plugins

		# docker exec -ti YOUR_WP_CONTAINER sh
		# echo "define('FS_METHOD','direct');" >>wp-config.php

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
and remove the named volume `wordpress-volume`

	# docker volume rm wordpress_wordpress-volume

to tear down the service.

Then change the wordpress service version in the compose file and start the container with

	# docker-compose up -d

Lastly do the [Manual steps](#markdown-header-manual-steps) and you have upgraded your wordpress service.

### Legacy configs

This has to be put somewhere so it is persistent on container rebuilt (otherwise MariaDB will not work)

    # echo "extension=pdo_mysql.so;" >> /usr/local/etc/php/conf.d/php.ini

TODO investigate why is not needed anymore.


## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/wordpress/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__2.1__

* Updated readme
* Changed license
* Put into public bitbucket repo
* Updated wordpress container version so its only bound to php & alpine, not the wordpress version

__2.0__

* Separated the environment variables from the compose file

### Open issues

* I have to look into either one of the following solutions for installing & upgrading plugins, etc.
  * Establish an (s)ftp via an ssh tunneling into the container
  * Automatically add `define('FS_METHOD','direct');` to `wp-config.php`
* You need to chmod 777 the mounted files / dirs in wp-content because otherwise Wordpress is not able to install / update plugins

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Remove Docker Volumes](https://docs.docker.com/engine/reference/commandline/volume_rm/)
* [Nginx Image](https://hub.docker.com/_/nginx)
* [Wordpress Image](https://hub.docker.com/_/wordpress)
* [Mariadb Image](https://hub.docker.com/r/yobasystems/alpine-mariadb)
